extends Spatial

onready var TRANSITION_TIME = 1.5
onready var sceneToLoad = preload("res://BattleDisplay/BattleScenePlatforms.tscn")

func _on_BadRed_diamond_trans():
	$Tween.interpolate_property($CanvasLayer/ColorRect.material, "shader_param/progress", 0, 1, TRANSITION_TIME) 
	$Tween.start()
	yield(get_tree().create_timer(TRANSITION_TIME), "timeout")
	get_tree().change_scene_to(sceneToLoad)
