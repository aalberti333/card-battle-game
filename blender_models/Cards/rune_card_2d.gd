extends Node2D

# First card begins at (1080, 392)
# Cards from there increment by 136 on x and stay same on y
# ex: (1216, 392) -> (1352, 392) -> (1488, 392), etc.

# MUST BE 2x THE TIME IN "glicth_space.gd"!!!!
onready var WAIT_FOR_GLITCH_TIME = 1

var spell_cast setget spell_cast_set, spell_cast_get

signal spell

# Have glitch shader disabbled at beginning,
# then enable again.
# Transition opacity from back to fireball rapidly
# for x seconds, then turn off glitch shader

func _ready():
	yield(get_tree().create_timer(WAIT_FOR_GLITCH_TIME), "timeout")
	$back.visible = false

func _on_TextureRect_mouse_entered():
	self.scale = Vector2(1.3, 1.3)
	self.position.x -= 15

func _on_TextureRect_mouse_exited():
	self.scale = Vector2(1, 1)
	self.position.x += 15	

func _on_TextureRect_gui_input(event):
	if  event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		emit_signal("spell")
		
func set_card(card_texture_prop):
	$spell.set_texture(card_texture_prop)
	
func spell_cast_set(new_value):
	spell_cast = new_value
	
func spell_cast_get():
	return spell_cast
