extends StaticBody

var left_available = false
var left_platform

var right_available = false
var right_platform

var up_available = false
var up_platform

var down_available = false
var down_platform

func _on_AreaUp_area_entered(area):
	up_platform = area.get_parent()
	up_available = true

func _on_AreaDown_area_entered(area):
	down_platform = area.get_parent()
	down_available = true

func _on_AreaRight_area_entered(area):
	right_platform = area.get_parent()
	right_available = true
	
func _on_AreaLeft_area_entered(area):
	left_platform = area.get_parent()
	left_available = true
