extends KinematicBody

var SHOOT_SPEED = 25
var fire_now = false
var target_position : Vector3

func _ready():
	# To follow player when player moves (use as a laser attack or something)
	# you could set this to false
	set_as_toplevel(true)
	visible = false

func deploy_shot(enemy_position):
	target_position = enemy_position
	fire_now = true

func _physics_process(delta):
	if fire_now:
		visible = true
		$AnimationPlayer.play("shoot")
		var collide = move_and_collide(target_position * SHOOT_SPEED * delta)
		if collide:
			var status_list = collide.collider.get_node("StatusBar/CanvasStatus/StatusList")
			
			if !collide.collider.battle_status["burn"]:
				var fire = TextureRect.new()
				fire.texture = load("res://Icons/kenney_boardgameicons/PNG/Double (128px)/fire.png")
				fire.name = "burn_status"
				status_list.add_child(fire)
				var timer_manager = collide.collider.get_node("TimerManager")
				var timer = timer_manager.get_node("TimerBase")
				timer.set("wait_time", 3)
				timer.set("one_shot", true)
				timer.start()
				timer_manager.add_child(timer)
				collide.collider.set_status("burn")
			
			# TODO: Check to make sure of enemy type
			var health_bar = collide.collider.get_node("StatusBar/CanvasStatus/HealthBar")
			var health_progress = health_bar.get_node("HealthProgress")
			
			var new_health = health_progress.value - $AttackStats.damage
			health_bar.update_healthbar(new_health)
			queue_free()
