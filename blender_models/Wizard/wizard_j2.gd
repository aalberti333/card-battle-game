extends KinematicBody

var gravity = Vector3.DOWN * 12
var speed = 4
var jump_speed = 6
var spin = 0.1

onready var grassland = get_node("/root/Spatial/Spatial/Grassland/MultiMeshInstance")

var velocity = Vector3()
var jump = false

func _process(delta):
	grassland.material_override.set_shader_param("player_pos", global_transform.origin)
	

func _physics_process(delta):
	velocity += gravity * delta
	get_input()
	velocity = move_and_slide(velocity, Vector3.UP)
	if jump and is_on_floor():
		velocity.y = jump_speed

func get_input():
	# vy used to preserve gravity
	var vy = velocity.y
	velocity = Vector3()
	if Input.is_action_pressed("move_forward"):
		# negative because forward is negative z direction
		# basis controls the rotation of a transform
		velocity += -transform.basis.z * speed
		$AnimationPlayer.play("Run")
	if Input.is_action_pressed("move_back"):
		velocity += transform.basis.z * speed
	if Input.is_action_pressed("strafe_right"):
		velocity += transform.basis.x * speed
	if Input.is_action_pressed("strafe_left"):
		velocity += -transform.basis.x * speed
	velocity.y = vy
	jump = false
	if Input.is_action_just_released("move_forward"):
		$AnimationPlayer.play("0InitPose")
	if Input.is_action_just_pressed("jump"):
		jump = true

func _input(event):
	if event is InputEventMouseMotion:
		# negative to get mouse direction correct
		rotate_y(-lerp(0, spin, event.relative.x/10))
