extends Spatial

onready var ROTATE_RADS = .0001
onready var ROTATE_UPPER_BOUND = 0.1
onready var ROTATE_LOWER_BOUND = 0
onready var go_right = true
onready var current_platform = $Spatial/platform_base_BC
var y_ang
enum {UP, DOWN, RIGHT, LEFT}

func _ready():
	$Spatial/wizard_battle_j2.enemy_vec_set($Spatial/BadRed_battle.global_transform.origin - global_transform.origin)
	
func _process(delta):
	y_ang = $Spatial/CameraHub.transform.basis.get_euler()[1]
	rotate_camera(y_ang, ROTATE_RADS)

func rotate_camera(y_angle, rotate_radian_amount):
	if y_angle < ROTATE_UPPER_BOUND and y_angle >= ROTATE_LOWER_BOUND and go_right:
		$Spatial/CameraHub.rotate_y(rotate_radian_amount)
	if y_angle >= ROTATE_UPPER_BOUND and go_right:
		go_right = false
		$Spatial/CameraHub.rotate_y(-rotate_radian_amount)
	if y_angle < ROTATE_UPPER_BOUND and not go_right:
		$Spatial/CameraHub.rotate_y(-rotate_radian_amount)
	if y_angle <= ROTATE_LOWER_BOUND and not go_right:
		go_right = true
		$Spatial/CameraHub.rotate_y(rotate_radian_amount)
	if y_angle < ROTATE_LOWER_BOUND and go_right:
		$Spatial/CameraHub.rotate_y(rotate_radian_amount)


func _on_wizard_battle_j2_platform_availability(direction):
	if direction == UP and current_platform.up_available:
		$Spatial/wizard_battle_j2.global_transform.origin = current_platform.up_platform.global_transform.origin
		current_platform = current_platform.up_platform
	if direction == DOWN and current_platform.down_available:
		$Spatial/wizard_battle_j2.global_transform.origin = current_platform.down_platform.global_transform.origin
		current_platform = current_platform.down_platform
	if direction == RIGHT and current_platform.right_available:
		$Spatial/wizard_battle_j2.global_transform.origin = current_platform.right_platform.global_transform.origin
		current_platform = current_platform.right_platform
	if direction == LEFT and current_platform.left_available:
		$Spatial/wizard_battle_j2.global_transform.origin = current_platform.left_platform.global_transform.origin
		current_platform = current_platform.left_platform
