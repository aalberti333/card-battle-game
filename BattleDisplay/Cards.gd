extends Node2D

onready var fireball_texture = load("res://blender_models/Spells/Fireball/fireball_render_s.PNG")
onready var poison_texture = load("res://blender_models/Spells/Poison/poison_render.png")

onready var deck = ["fireball", "fireball", "fireball", "poison", "poison"]
onready var lookup_table = {
	"fireball":
		{"texture": fireball_texture, "x_position": 30.939, "y_position": 48.354, "x_scale": 0.05, "y_scale": 0.05},
	"poison":
		{"texture": poison_texture, "x_position": 30.939, "y_position": 48.354, "x_scale": 3, "y_scale": 3},
}

onready var hand_size = 5
onready var hand : Array

signal spell_action(spell_name)

func _ready():
	randomize()
	deck.shuffle()
	
	for val in hand_size:
		hand.append(deck.pop_front())
	
	var cards = get_children()
	
	for i in len(cards) - 1:
		cards[i].set_card(lookup_table[hand[i]]["texture"])
		cards[i].spell_cast_set(hand[i])

func _on_rune_card_2d_spell():
	emit_signal("spell_action", $rune_card_2d.spell_cast_get())
	$rune_card_2d.visible = false

func _on_rune_card_2d2_spell():
	emit_signal("spell_action", $rune_card_2d2.spell_cast_get())
	$rune_card_2d2.visible = false

func _on_rune_card_2d3_spell():
	emit_signal("spell_action", $rune_card_2d3.spell_cast_get())
	$rune_card_2d3.visible = false

func _on_rune_card_2d4_spell():
	emit_signal("spell_action", $rune_card_2d4.spell_cast_get())
	$rune_card_2d4.visible = false

func _on_rune_card_2d5_spell():
	emit_signal("spell_action", $rune_card_2d5.spell_cast_get())
	$rune_card_2d5.visible = false
