extends KinematicBody

signal platform_availability(direction)

onready var fireball_scene = preload("res://blender_models/Spells/Fireball/fireball.tscn")
var enemy_position setget enemy_vec_set
enum {UP, DOWN, RIGHT, LEFT}

func _ready():
	set_process_input(true)

func _physics_process(delta):
	get_input()
	
func enemy_vec_set(enemy_vec_position: Vector3):
	enemy_position = enemy_vec_position

func shoot_target(spell_name: String):
	# TODO: remove original fireball from scene after determining animations/scale/etc
	match spell_name:
		"fireball":
			var instance_fireball_scene = fireball_scene.instance()
			instance_fireball_scene.name = str(spell_name, OS.get_unique_id())
			$Spells.add_child(instance_fireball_scene)
			
			var unique_fireball = get_node(str("Spells/", instance_fireball_scene.name))
			# This previously deployed to enemy_position. However, this would
			# try and track down the enemy. That could work for a homing attack,
			# but the vector to follow is currently set in the _ready() of the
			# root node. This would need to be updated dynamically, and player
			# would need an option to choose the target if this were implemented.
			# For now, just shoot straight forward.
			unique_fireball.deploy_shot(Vector3.FORWARD)

func get_input():
	# move player to direction
	if Input.is_action_just_pressed("move_forward"):
		emit_signal("platform_availability", UP)
	if Input.is_action_just_pressed("move_back"):
		emit_signal("platform_availability", DOWN)
	if Input.is_action_just_pressed("strafe_right"):
		emit_signal("platform_availability", RIGHT)
	if Input.is_action_just_pressed("strafe_left"):
		emit_signal("platform_availability", LEFT)

func _on_Cards_spell_action(spell_name: String):
	$AnimationPlayer.playback_speed = 2.0
	$AnimationPlayer.play("Cast0")
	shoot_target(spell_name)
