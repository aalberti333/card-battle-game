extends Node

signal max_changed(new_max)
signal changed(new_amount)
signal depleted

export (int) var max_amount = 100 setget set_max
onready var current = 0 setget set_value

onready var bar = self.get_node("TextureProgress")
onready var bar2 = self.get_node("TextureProgress2")
onready var bar3 = self.get_node("TextureProgress3")

onready var bar_counter = 1
onready var count_to_bar = {1: bar, 2: bar2, 3: bar3}
onready var COUNT_BAR_HALT = len(count_to_bar) + 1

var seg_to_fill
var fillres

var maxed_bar = false

func _ready():
	seg_to_fill = count_to_bar[bar_counter]
	_initialize()
	
func _process(delta):
	if bar_counter < COUNT_BAR_HALT:
		fillres = _fillbar(current, seg_to_fill)
		current = fillres["current"]
		seg_to_fill = fillres["seg_to_fill"]

func set_max(new_max):
	max_amount = new_max
	max_amount = max(1, new_max)
	emit_signal("max_changed", max_amount)

func set_value(new_value):
	current = new_value
	current = clamp(current, 0, max_amount)
	emit_signal("changed", current)

	if current == 0:
		emit_signal("depleted")

func _initialize():
	emit_signal("max_changed", max_amount)
	emit_signal("changed", current)
	
func _fillbar(current, seg_to_fill):
	# slowly fills the timebar
	current += 1
	# value for some reason maxes out at half of what max is
	# reset current, up the bar count
	if current == max_amount/2:
		current = 0
		bar_counter += 1
				
		if bar_counter < COUNT_BAR_HALT:
			seg_to_fill = count_to_bar[bar_counter]
		if bar_counter == COUNT_BAR_HALT:
			maxed_bar = true
			return {"current": current, "seg_to_fill": seg_to_fill}
	seg_to_fill.value = current
	return {"current": current, "seg_to_fill": seg_to_fill}
