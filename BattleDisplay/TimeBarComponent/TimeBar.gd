extends Node

signal max_changed(new_max)
signal changed(new_amount)
signal depleted

export (int) var max_amount = 100 setget set_max
onready var current = 0 setget set_value

func _ready():
	_initialize()
	
func _process(delta):
	_fillbar(current)

func set_max(new_max):
	max_amount = new_max
	max_amount = max(1, new_max)
	emit_signal("max_changed", max_amount)

func set_value(new_value):
	current = new_value
	current = clamp(current, 0, max_amount)
	emit_signal("changed", current)

	if current == 0:
		emit_signal("depleted")

func _initialize():
	emit_signal("max_changed", max_amount)
	emit_signal("changed", current)
	
func _fillbar(current):
	# slowly fills the timebar
	current += 1
	set_value(current)
