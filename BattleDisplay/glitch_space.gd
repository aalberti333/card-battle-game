extends ColorRect

# MUST BE HALF THE TIME IN "glicth_space.gd"!!!!
onready var WAIT_FOR_GLITCH_TIME = 0.5

func _ready():
	# overrides eyeball in the editor
	self.visible = false
	yield(get_tree().create_timer(WAIT_FOR_GLITCH_TIME), "timeout")
	self.visible = true
	yield(get_tree().create_timer(WAIT_FOR_GLITCH_TIME * 2.0), "timeout")
	self.visible = false
