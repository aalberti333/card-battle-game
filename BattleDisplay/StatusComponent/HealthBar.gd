extends Node2D

var bar_red = preload("res://BattleDisplay/StatusComponent/barHorizontal_red.png")
var bar_green = preload("res://BattleDisplay/StatusComponent/barHorizontal_green.png")
var bar_yellow = preload("res://BattleDisplay/StatusComponent/barHorizontal_yellow.png")

onready var health_progress = $HealthProgress
		
func _process(delta):
	global_rotation = 0
	
func update_healthbar(value):
	health_progress.texture_progress = bar_green
	if value < health_progress.max_value * 0.7:
		health_progress.texture_progress = bar_yellow
	if value < health_progress.max_value * 0.35:
		health_progress.texture_progress = bar_red
	health_progress.value = value
