extends KinematicBody
onready var stats = $Stats

signal diamond_trans

# TODO: add random movement for enemy
# enemy will need its own tiles, and will need to distinguish between
# the battle environment and the platform environment
# probably should create separate nodes for each scenario like what was done
# with the character

func _on_Area_body_entered(body):
	print(global_transform.origin)
	$Area/CollisionShape.disabled = true
	emit_signal("diamond_trans")
