extends KinematicBody

onready var health_progress = $StatusBar/CanvasStatus/HealthBar/HealthProgress
onready var status_list = $StatusBar/CanvasStatus/StatusList
#onready var time_manager = $TimerManager
onready var battle_status = {"burn": false, "poison": false}

func _ready():
	# this is based on the value of the collision layer, double check in the
	# editor to see what this is, pattern is 2^n for some reason (where n starts at 0)
	# Because this is the 3rd spot, we take 2^2 (indexed by zero, so 2) as the layer
	set_collision_layer(4)

	# grabs max_health from the stats attribute and sets it as the max_value for the 
	# health progress bar. Note: 'health' FROM STATS CAN NOT BE ABOVE 10000 FOR THE CURRENT IMPLEMENTATION
	# that would need to be changed in $HealthProgress's max_value field.
	health_progress.max_value = $Stats.health

func set_status(status):
	if status in battle_status:
		battle_status[status] = true


func _on_TimerBase_timeout():
	battle_status["burn"] = false
	for obj in status_list.get_children():
		if obj.name == "burn_status":
			remove_child(obj)
			obj.queue_free()
