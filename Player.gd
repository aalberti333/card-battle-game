tool
extends MeshInstance

func _process(delta):
	get_parent().get_node("MultiMeshInstance").material_override.set_shader_param("player_pos", global_transform.origin)
