extends Spatial

onready var start_tween = true

func _process(delta):
	if start_tween:
		$Tween.interpolate_property($CanvasLayer/ColorRect.material, "shader_param/progress", 0, 1, 1) 
		$Tween.start()
		start_tween = false
		print("STARTED")
